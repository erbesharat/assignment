const https = require("https");
const crypto = require("crypto");
const queryString = require("./queryString");

const Request =
  ({ apiKey, secretKey, baseURL }) =>
  (endpoint, method, qs, hmac) =>
    new Promise((resolve, reject) => {
      let stringQS = "";
      if (qs) {
        stringQS = queryString(qs);
      }
      let url = `${baseURL}${endpoint}${stringQS ? `?${stringQS}` : ""}`;

      if (hmac) {
        const signature = crypto
          .createHmac("sha256", secretKey)
          .update(stringQS)
          .digest("hex");

        url = `${url}&signature=${signature}`;
      }

      const request = https.request(
        url,
        {
          method,
          headers: {
            "X-MBX-APIKEY": apiKey,
          },
        },
        (response) => {
          let data = "";
          response.on("data", (chunk) => {
            data = data + chunk.toString();
          });

          response.on("end", () => {
            try {
              const response = JSON.parse(data);
              resolve(response);
            } catch {
              reject(data);
            }
          });
        }
      );

      request.on("error", (error) => {
        reject(error);
      });

      request.end();
    });

module.exports = Request;
