const Request = require("./request");
const Websocket = require("./websocket");

const API_HOST = process.env.API_HOST || "https://testnet.binance.vision";
const WS_HOST = process.env.WS_HOST || "wss://testnet.binance.vision/ws";
const API_KEY = process.env.API_KEY;
const SECRET_KEY = process.env.SECRET_KEY;

const createRequest = Request({
  baseURL: API_HOST,
  apiKey: API_KEY,
  secretKey: SECRET_KEY,
});
const createWebsocket = Websocket(WS_HOST);

const generateListenKey = async () => {
  const response = await createRequest("/api/v3/userDataStream", "POST");

  let pingTimeout;
  const ping = async () => {
    createRequest("/api/v3/userDataStream", "PUT");
    pingTimeout = setTimeout(ping, 30 * 60 * 1000);
  };

  pingTimeout = setTimeout(ping, 30 * 60 * 1000);

  const close = async () => {
    await createRequest("/api/v3/userDataStream", "DELETE");
    clearTimeout(pingTimeout);
  };

  return [response.listenKey, close];
};

const createUserDataWebsocket = async (onMessage) => {
  const [listenKey] = await generateListenKey();
  await createWebsocket(`/${listenKey}`, onMessage);
};

const createTradeOrder = async () => {
  console.log("Creating new order");
  try {
    const response = await createRequest(
      "/api/v3/order",
      "POST",
      {
        symbol: "BTCUSDT",
        side: "BUY",
        type: "MARKET",
        quantity: 0.1,
        timestamp: Date.now(),
      },
      true
    );
    console.log("response", response);
  } catch (err) {
    console.error(err);
    process.exit(0);
  }
};

const trader = () =>
  setTimeout(() => {
    createTradeOrder();
    trader();
  }, 10000);

const start = async () => {
  await createUserDataWebsocket((data) => {
    try {
      const response = JSON.parse(data.toString());
      if (response.e !== "executionReport" && response.x === "TRADE") {
        return;
      }

      const timestamp = response.T;
      if (!timestamp) {
        return;
      }

      const symbol = response.s;
      const quantity = response.q;
      const price = response.p;

      console.log("Timestamp\t\t\tSymbol\tQuantity\tPrice");
      console.log(
        `${new Date(timestamp).toISOString()}\t${symbol}\t${quantity}\t${price}`
      );
    } catch (err) {
      console.error(err);
      process.exit(0);
    }
  });

  console.log("Starting trader");
  createTradeOrder();
  trader();
};

start();
