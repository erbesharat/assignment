# Binance NodeJS Assignment
This project uses the provided API credentials to open up a websocket and start creating orders on the SPOT test net.

# Running
In order to run the project you have to first set these two environment variables:
* `API_KEY`
* `SECRET_KEY`

Values can be created using [THIS](https://testnet.binance.vision/) link.

Install the dependencies using:
```
npm i
```

And then use the following command to run the script:
```
API_KEY=? SECRET_KEY=? node index.js
```