const ws = require("ws");

const Websocket = (host) => (url, onMessage) =>
  new Promise((resolve, reject) => {
    const socket = new ws(`${host}${url}`);
    let connected = false;

    socket.on("open", () => {
      console.log("Connected");
      connected = true;
      resolve(socket);
    });

    socket.on("message", (data) => {
      onMessage(data);
    });

    socket.on("close", () => {
      console.log("Disconnected");
      if (!connected) {
        reject();
      }
    });
  });

module.exports = Websocket;
